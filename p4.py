import datetime
import torch
import errno
import math
import sys
import os
import re

# device = torch.device("cuda")
device = torch.device("cpu")
dtype = torch.float32

def time_stamp():
    t = datetime.datetime.utcnow().strftime("[%Y-%m-%d %H:%M:%S.%f]:")
    return t

def S(ka, la, phi):
    N,T,L = phi.shape
    t = torch.arange(T)
    x = torch.arange(L)
    t,x = torch.meshgrid(t, x)
    phi = phi.reshape([-1, T*L])
    x00 = (t*L + x).flatten()
    x0p = (t*L + (x+1)%L).flatten()
    xp0 = (((t+1)%T)*L + x).flatten()
    kin = -2*ka*phi*(phi[:,x0p] + phi[:,xp0])
    pot = (1-2*la)*phi**2 + la*phi**4
    return (kin + pot).sum(axis=1)

def al(nn, eo, z):
    """given e0 = 0 or 1
    applies the generative model g(z)=f
    returns the field (half of it unchanged because of NICE and half of it transformed with nn)
    after the application of the grenerative model nn"""
    N,T,L = z.shape
    t,x = torch.meshgrid(torch.arange(T), torch.arange(L))
    i0 = ((t + x) % 2 == eo).tolist()
    i1 = ((t + x) % 2 != eo).tolist()
    zeo = (z[:,i0],z[:,i1])
    phi0 = zeo[0] + nn(zeo[1])
    phi1 = zeo[1]
    idx0 = t[torch.as_tensor(i0)]*L + x[torch.as_tensor(i0)]
    idx1 = t[torch.as_tensor(i1)]*L + x[torch.as_tensor(i1)]
    phi = torch.scatter(torch.zeros(N,T*L, device=device), 1, torch.stack([idx0]*N).to(device), phi0)
    phi = torch.scatter(phi, 1, torch.stack([idx1]*N).to(device), phi1)
    return phi.reshape(N,T,L)

def new_nn(Ns):
    mo = list()
    for i,N in enumerate(Ns[:-1]):
        mo.append(torch.nn.Linear(Ns[i], Ns[i+1], bias=False))
        mo.append(torch.nn.Tanh())

    nn = torch.nn.Sequential(*mo)
    return nn.to(device)

def jac(scs, nns, z):
    """returns the determinant of the transformation"""
    N,T,L = z.shape
    return -scs.reshape([-1,T*L]).sum(axis=1)

# def

def logqf(scs, nns, z):
    """returns KL(p(z)|q(f))=KL(|flow)=int p ln(p/q) = int p(z) ln(p(z)) - int p ln(q)
     - int p ln(q) = - 1/N Σ ln q(f) = - 1/N Σ ln q(g(z))"""
    N,T,L = z.shape
    logrz = - ((L*T)/2)*math.log(2*math.pi) - 0.5*z.pow(2).reshape([N,T*L]).sum(dim=1)
    logdt = jac(scs, nns, z)
    return logrz + logdt

# def logpz(scs, nns, z):


def apply_als(scs, nns, z):
    """applies NICE over all the 6 layered networks g(g(g(g(z))))=f choosing
    alternate subsets of z to mix all the points of the lattice returning the
    field distribution"""
    p = torch.as_tensor(z)
    for i,nn in enumerate(nns):
        p = al(nn, i % 2, p)
    return torch.exp(scs)*p

def r_acc(scs, nns, T, L, ka, la, N):
    """
    returns:
    1) the number of accepted training samples divided by the sample size
    2) the field"""
    z = torch.normal(mean=0, std=1, size=[N, T, L], dtype=dtype, device=device)
    with torch.no_grad():
        phi = apply_als(scs, nns, z)
        acs = S(ka, la, phi).tolist()
        lqs = logqf(scs, nns, z).tolist()
        ac0,lq0 = acs[0],lqs[0]
        iac = [0]
        acc = 0
        for i in range(1, N):
            ac,lq = acs[i],lqs[i]
            d = (ac+lq) - (ac0+lq0)
            if d < 0:
                acc += 1
                ac0,lq0 = ac,lq
                iac.append(i)
            # inspired by metropolis
            elif math.exp(-d) > torch.rand(()).item():
                acc += 1
                ac0,lq0 = ac,lq
                iac.append(i)
            else:
                iac.append(iac[-1])
    return acc/N,phi[iac,...]

T,L = 16,8
V = T*L
i = 0
if True: ### Start new training
    """network designed to take as input half the points of the lattice"""
    nns = [new_nn([V//2, V, V, V//2]) for i in range(6)]
    """lattice points"""
    scs = torch.randn([T,L], device=device, requires_grad=True, dtype=dtype)
if False: ## Load from file
    fname = "ka0p100000/iter000000029000.pickle"
    mo = torch.load(fname)
    nns,scs = mo
    i = int(re.match("iter([0-9]*)", os.path.basename(fname).split(".")[0]).groups()[0])

N = 4_096 # training samples?
ka,la = 0.1, 0.022
lr = 2e-4
print_every = 10
r_acc_every = 100
store_every = 1_000
optimizer = torch.optim.Adam(sum([list(nn.parameters()) for nn in nns], [scs]), lr=lr)
for i in range(i+1, 10_000_000):
    z = torch.randn(N,T,L, device=device)
    phi = apply_als(scs, nns, z)
    act = S(ka, la, phi)
    loss = (logqf(scs, nns, z) + act)
    ave,std = loss.mean(),loss.std()
    if i % print_every == 0:
        l = ave.item()
        v = std.item()
        v = abs((v/l))
        t = time_stamp()
        s = f" {t} i = {i:8.0f}   lo = {l:+7.5e}   N = {N:6.0f}   lr = {lr:4.2e}"
        if i % r_acc_every == 0:
            r,_ = r_acc(scs, nns, T, L, ka, la, 16_384)
            s += f"   r = {r:6.4f}"
            if r > 0.75:
                print(s)
                break
        print(s)
    if i % store_every == 0:
        dn = f"ka{ka:8.6f}".replace(".", "p")
        try:
            os.makedirs(dn)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise
        fn = f"iter{i:012.0f}.pickle"
        fname = f"{dn}/{fn}"
        torch.save([nns, scs], fname)
        print(f" > Checkpoint: {fname}")
    optimizer.zero_grad()
    ave.backward()
    optimizer.step()
